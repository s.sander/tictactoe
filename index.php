<?php
session_start();
?>
<!DOCTYPE html>
<html Lang="DE">
<head>
    <meta charset="utf-8">
    <title>Tic-Tac-Toe</title>
    <meta name="description" content="Tic-Tac-Toe-Game. Here is a short description for the page. This text is displayed e. g. in search engine result listings.">
    <style>
        table.tic td {
            border: 1px solid #333; /* grey cell borders */
            width: 8rem;
            height: 8rem;
            vertical-align: middle;
            text-align: center;
            font-size: 4rem;
            font-family: Arial;
        }
        table { margin-bottom: 2rem; }
        input.field {
            border: 0;
            background-color: white;
            color: white; /* make the value invisible (white) */
            height: 8rem;
            width: 8rem !important;
            font-family: Arial;
            font-size: 4rem;
            font-weight: normal;
            cursor: pointer;
        }
        input.field:hover {
            border: 0;
            color: #c81657; /* red on hover */
        }
        .colorX { color: #e77; } /* X is light red */
        .colorO { color: #77e; } /* O is light blue */
        table.tic { border-collapse: collapse; }
    </style>
</head>
<body>
    <section>
        <h1>Tic Tac Toe</h1>
        <article id="mainContent">
            <h3>how to play this game:</h3>
            <p>...well everybody should know how to play TicTacToe</p>
            <form method="get" action="index.php">

<!--PHP Starts here-->
<?php
/*class Player

{
	public $symbol;
	public $isturn;
} 

class Board
{
}

class TicTacToe
{
}
*/
$player = 'X'; //instead of this later the mathod getIsTurn of the Players

	$board = array();
	$board = array(array("","",""),
					array("","",""), 
					array("","",""));
					
					
if(isset($_SESSION['tictactoe']))
{	$board = $_SESSION['tictactoe'];
	
	for ($i = 0; $i < count($board); $i++)
	{
		for ($j = 0; $j < count($board[$i]); $j++)
		{
        if(isset($_GET["cell-".$i."-".$j]) ){
            $board[$i][$j] = $_GET["cell-".$i."-".$j];
            if ($_GET["cell-".$i."-".$j] == "X") {
                $player = "O";
            }else{
                $player = "X";
            }
		
        }
	} 		
	}		
}

$_SESSION["tictactoe"] = $board;

//Output
echo '<table class="tic">'."\n";

for ($i = 0; $i < count($board); $i++)
	{
	echo '<tr>'."\n";
	for ($j = 0; $j < count($board[$i]); $j++)
		{		
			if ($board [$i][$j] === "")
			{
				echo "\t".'<td><input type="submit" class="field" name="cell-'.$i.'-'.$j.'"value='.$player.'></td>'."\n";
			}
			else
			{
				echo "\t".'<td><span class="color'.$board[$i][$j].'">'.$board[$i][$j].'</span></td>'."\n";
			}
		} 
		
	echo '</tr>'."\n";	
	}
?>
                </table>
            </form>
        </article>
    </section>
</body>
</html>
